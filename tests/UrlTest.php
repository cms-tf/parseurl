<?php

    namespace CmsTf\ParseUrl\Tests;

    use CmsTf\ParseUrl\Url;
    use PHPUnit\Framework\TestCase;

    /**
     * Class UrlTest
     *
     * @package CmsTf\ParseUrl\Tests
     * @author Gerard Smit <gsmit1996@gmail.com>
     */
    class UrlTest extends TestCase {
        /**
         * @return array
         */
        public function parseProvider() {
            return array(
                array(
                    'http://ionicabizau.net/blog',

                    array(
                        'protocols' => array(
                            'http',
                        ),
                        'protocol' => 'http',
                        'port' => null,
                        'resource' => 'ionicabizau.net',
                        'user' => '',
                        'pathname' => '/blog',
                        'hash' => '',
                        'search' => '',
                    ),
                ),

                array(
                    '//ionicabizau.net/foo.js',

                    array(
                        'protocols' => array(),
                        'protocol' => '',
                        'port' => null,
                        'resource' => 'ionicabizau.net',
                        'user' => '',
                        'pathname' => '/foo.js',
                        'hash' => '',
                        'search' => '',
                    ),
                ),

                array(
                    'http://domain.com/path/name?foo=bar&bar=42#some-hash',

                    array(
                        'protocols' => array(
                            'http',
                        ),
                        'protocol' => 'http',
                        'port' => null,
                        'resource' => 'domain.com',
                        'user' => '',
                        'pathname' => '/path/name',
                        'hash' => 'some-hash',
                        'search' => 'foo=bar&bar=42',
                    ),
                ),

                array(
                    'http://domain.com/path/name#some-hash?foo=bar&bar=42',

                    array(
                        'protocols' => array(
                            'http',
                        ),
                        'protocol' => 'http',
                        'port' => null,
                        'resource' => 'domain.com',
                        'user' => '',
                        'pathname' => '/path/name',
                        'hash' => 'some-hash?foo=bar&bar=42',
                        'search' => '',
                    ),
                ),

                array(
                    'git+ssh://git@host.xz/path/name.git',

                    array(
                        'protocols' => array(
                            'git',
                            'ssh',
                        ),
                        'protocol' => 'git',
                        'port' => null,
                        'resource' => 'host.xz',
                        'user' => 'git',
                        'pathname' => '/path/name.git',
                        'hash' => '',
                        'search' => '',
                    ),
                ),

                array(
                    'git@github.com:IonicaBizau/git-stats.git',

                    array(
                        'protocols' => array(),
                        'protocol' => 'ssh',
                        'port' => null,
                        'resource' => 'github.com',
                        'user' => 'git',
                        'pathname' => '/IonicaBizau/git-stats.git',
                        'hash' => '',
                        'search' => '',
                    ),
                ),
            );
        }

        /**
         * @link https://github.com/IonicaBizau/parse-url/blob/master/lib/index.js
         *
         * @dataProvider parseProvider
         *
         * @param string $url
         * @param array  $expected
         */
        public function testParse($url, $expected) {
            self::assertEquals($expected, Url::parse($url));
        }

        /**
         * @link https://github.com/IonicaBizau/protocols/blob/master/test/index.js
         */
        public function testProtocols() {
            self::assertEquals(array('git', 'ssh'), Url::getProtocols('git+ssh://git@some-host.com/and-the-path/name'));
            self::assertEquals(array(), Url::getProtocols('//foo.com/bar.js'));
            self::assertEquals(array('ssh'), Url::getProtocols('ssh://git@some-host.com/and-the-path/name'));
            self::assertEquals('git', Url::getProtocol('git+ssh://git@some-host.com/and-the-path/name'));
        }

        /**
         * @return array
         */
        public function sshProvider() {
            return array(
                array(
                    'ssh://user@host.xz:port/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://user@host.xz/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://host.xz:port/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://host.xz/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://user@host.xz/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://host.xz/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://user@host.xz/~user/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://host.xz/~user/path/to/repo.git/',
                    true,
                ),

                array(
                    'ssh://user@host.xz/~/path/to/repo.git',
                    true,
                ),

                array(
                    'ssh://host.xz/~/path/to/repo.git',
                    true,
                ),

                array(
                    'user@host.xz:/path/to/repo.git/',
                    true,
                ),

                array(
                    'user@host.xz:~user/path/to/repo.git/',
                    true,
                ),

                array(
                    'user@host.xz:path/to/repo.git',
                    true,
                ),

                array(
                    'host.xz:/path/to/repo.git/',
                    true,
                ),

                array(
                    'host.xz:path/to/repo.git',
                    true,
                ),

                array(
                    'host.xz:~user/path/to/repo.git/',
                    true,
                ),

                array(
                    'rsync://host.xz/path/to/repo.git/',
                    true,
                ),

                array(
                    'git://host.xz/path/to/repo.git/',
                    false,
                ),

                array(
                    'git://host.xz/~user/path/to/repo.git/',
                    false,
                ),

                array(
                    'http://host.xz/path/to/repo.git/',
                    false,
                ),

                array(
                    'https://host.xz/path/to/repo.git/',
                    false,
                ),

                array(
                    '/path/to/repo.git/',
                    false,
                ),

                array(
                    'path/to/repo.git/',
                    false,
                ),

                array(
                    '~/path/to/repo.git',
                    false,
                ),

                array(
                    'file:///path/to/repo.git/',
                    false,
                ),

                array(
                    'file://~/path/to/repo.git/',
                    false,
                ),

                array(
                    'git+ssh://git@some-host.com/and-the-path/name',
                    true,
                )
            );
        }

        /**
         * @link https://github.com/IonicaBizau/parse-url/blob/master/lib/index.js
         *
         * @dataProvider sshProvider
         *
         * @param string $url
         * @param array  $expected
         */
        public function testIsSsh($url, $expected) {
            self::assertEquals($expected, Url::isSsh($url));
        }
    }