<?php

    namespace CmsTf\ParseUrl;

    /**
     * Class Url
     *
     * @package CmsTf\ParseUrl
     * @author Gerard Smit <gsmit1996@gmail.com>
     */
    class Url {
        /**
         * @link https://github.com/IonicaBizau/protocols/blob/master/lib/index.js
         *
         * @param      $input
         *
         * @return array|string|null
         */
        public static function getProtocols($input) {
            $index = strpos($input, '://');
            return array_filter(explode('+', substr($input, 0, $index)));
        }

        /**
         * Get the protocol.
         *
         * @param $input
         *
         * @return mixed|string
         */
        public static function getProtocol($input) {
            $protocols = self::getProtocols($input);

            if (count($protocols) > 0) {
                return $protocols[0];
            }

            if (self::isSsh($input)) {
                return 'ssh';
            }

            if ($input[1] === '/') {
                return '';
            }

            return 'file';
        }

        /**
         * @link https://github.com/IonicaBizau/is-ssh/blob/master/lib/index.js
         *
         * @param $input
         *
         * @return bool
         */
        public static function isSsh($input) {
            if (is_array($input)) {
                return in_array('ssh', $input, true) || in_array('rsync', $input, true);
            }

            if (!is_string($input)) {
                return false;
            }

            if (self::isSsh(self::getProtocols($input))) {
                return true;
            }

            $input = substr($input, strpos($input, '://'));
            return strpos($input, '@') < strpos($input, ':');
        }

        /**
         * @link https://github.com/IonicaBizau/parse-url/blob/master/lib/index.js
         *
         * @param $url
         *
         * @return array
         */
        public static function parse($url) {
            $output = array(
                'protocols' => self::getProtocols($url),
                'protocol' => null,
                'port' => null,
                'resource' => '',
                'user' => '',
                'pathname' => '',
                'hash' => '',
                'search' => ''
            );

            $output['protocol'] = self::getProtocol($url);

            if ($output['protocol'] === '') {
                $url = substr($url, 2);
            }

            $protocolIndex = strpos($url, '://');
            if ($protocolIndex !== false) {
                $url = substr($url, $protocolIndex + 3);
            }

            $parts = explode('/', $url);
            $output['resource'] = array_shift($parts);

            // user@domain
            $splits = explode('@', $output['resource']);
            if (count($splits) === 2) {
                list($output['user'], $output['resource']) = $splits;
            }

            // domain.com:port
            $splits = explode(':', $output['resource']);
            if (count($splits) === 2) {
                list($output['resource'], $output['port']) = $splits;

                if(!is_int($output['port'])) {
                    $output['port'] = null;
                    array_unshift($parts, $splits[1]);
                }
            }

            // Remove empty elements
            $parts = array_filter($parts);

            // Stringify the pathname
            $output['pathname'] = '/' . implode('/', $parts);

            // #some-hash
            $splits = explode('#', $output['pathname']);
            if (count($splits) === 2) {
                list($output['pathname'], $output['hash']) = $splits;
            }

            // ?foo=bar
            $splits = explode('?', $output['pathname']);
            if (count($splits) === 2) {
                list($output['pathname'], $output['search']) = $splits;
            }

            return $output;
        }
    }